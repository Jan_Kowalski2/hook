import React, { Fragment } from "react";
import { BrowserRouter, Switch, Route } from "react-router-dom";

import { HomePage, PostPage, StandardPage } from "./views";
import { Nav, Footer } from "./components";

function Router(): JSX.Element {
  return (
    <BrowserRouter>
      <Fragment>
        <Nav />
        <Switch>
          <Route path="/posts/:id" component={PostPage} />
          <Route exact path="/" component={HomePage} />
          <Route path="/" component={StandardPage} />
        </Switch>
        <Footer />
      </Fragment>
    </BrowserRouter>
  );
}

function App(): JSX.Element {
  return (
    <Router />
  );
} 

export default App;
