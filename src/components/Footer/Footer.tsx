import React from "react";

import styles from './Footer.module.css';

function Footer(): JSX.Element {
    return (
        <footer className={styles.root}>
            <section className={styles.bio}>
                <div className="container">
                    <h2>Co nieco o mnie.</h2>
                    <p>Lorem ipsum dolores sit amet.</p>
                </div>
            </section>
            <section className={styles.copyright}>
                <div className="container">
                    <p>&copy; <strong>Faaak!</strong>, Piotr Kwiatek.</p>
                </div>
            </section>
        </footer>
    );
}

export default Footer;