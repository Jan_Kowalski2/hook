import React from "react";
import { Link } from "react-router-dom";

import styles from './Header.module.css';

function Header(): JSX.Element {
  return (
    <header className={styles.root}>
      <div className="container">
        <h1><Link to="/">Faaak!</Link></h1>
      </div>
    </header>
  );
}

export default Header;