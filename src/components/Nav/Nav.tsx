import React, { useState } from "react";
import { Link } from "react-router-dom";
import { useFetchStatuses } from "hooks/fetchStatus";

import styles from './Nav.module.css';

function Nav(): JSX.Element {
  const [links, handleLinks] = useState<any[]>([]);
  const fetchStatus = useFetchStatuses("/pages", (response: any) => {
    handleLinks(response.json);
  });
  
  const renderLinks = () => {
    if (fetchStatus.isError) {
      return null;
    }
    if (fetchStatus.isLoaded) {
      return (
        <ul className={styles.nav}>
          {links.map(item => <li key={item.slug}><Link to={item.slug} title={item.title.rendered}>{item.title.rendered}</Link></li>)}
        </ul>
      )
    }
    return <div className={styles.navLoading}>Ładowanie...</div>
  }

  return (
    <nav className={styles.root}>
      <div className="container">
        <h1><Link to="/">Faaak!</Link></h1>
        {renderLinks()}
      </div>
    </nav>
  );
}

export default Nav;