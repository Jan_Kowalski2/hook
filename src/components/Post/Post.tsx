import React, { Fragment } from "react";
import { Link } from "react-router-dom";
import { getDate, getRandomBackgroundColor } from "helpers";
import { FetchStatus } from "constants/index";

import styles from "./Post.module.css";

interface Post {
  isExtended: boolean;
  id: number;
  slug: string;
  title: { rendered: string };
  excerpt: { rendered: string };
  content: { rendered: string };
  author: number;
  modified: string;
  _embedded: any;
  fetchStatus: FetchStatus;
}

function Post(props: Post): JSX.Element {
  const {
    isExtended,
    id,
    slug,
    title,
    excerpt,
    content,
    modified,
    fetchStatus
  } = props;

  if (fetchStatus.isLoading) {
    return (
      <article className={styles.root}>
        <div className={styles.articleImageLoading}></div>
        <div className={styles.articleHolderLoading}>
          <h2>Artykuł jest pobierany...</h2>
          <div className={styles.articleContentLoading}>Ładowanie...</div>
        </div>
      </article>
    );
  }

  const destination = `/posts/${id}/${slug}`;
  const meta = props["_embedded"];

  let imageUrl = "";
  let author = {
    name: "Anon",
    imageUrl: ""
  };

  if (meta["wp:featuredmedia"]) {
    imageUrl =
      meta["wp:featuredmedia"][0]["media_details"]["sizes"]["full"][
        "source_url"
      ];
  }

  if (meta["author"]) {
    author = {
      name: meta["author"][0]["name"],
      imageUrl: meta["author"][0]["avatar_urls"][24]
    };
  }

  if (isExtended) {
    return (
      <article className={styles.root}>
        <div className={styles.articleImage}>
          {renderPostImage(imageUrl, title.rendered)}
        </div>
        <div className={styles.articleHolder}>
          <h2>{title.rendered}</h2>
          <p className={styles.meta}>
            {renderAuthor(author.imageUrl, author.name)},{" "}
            {getDate(modified, true)}
          </p>
          <div
            className={styles.articleContent}
            dangerouslySetInnerHTML={{ __html: content.rendered }}
          />
        </div>
      </article>
    );
  }

  return (
    <article className={styles.root}>
      <Link
        to={destination}
        title={title.rendered}
        className={styles.articleImage}
      >
        {renderPostImage(imageUrl, title.rendered)}
      </Link>
      <div className={styles.articleHolder}>
        <h2>
          <Link to={destination} title={title.rendered}>
            {title.rendered}
          </Link>
        </h2>
        <p className={styles.meta}>
          {renderAuthor(author.imageUrl, author.name)},{" "}
          {getDate(modified, true)}
        </p>
        <div
          className={styles.articleContent}
          dangerouslySetInnerHTML={{ __html: excerpt.rendered }}
        />
        <div className="textRight">
          <Link
            to={destination}
            title={title.rendered}
            className={styles.readMore}
          >
            Czytaj dalej
          </Link>
        </div>
      </div>
    </article>
  );
}

function renderAuthor(imageUrl: string, name: string) {
  if (imageUrl.length > 0) {
    return (
      <Fragment>
        <img src={imageUrl} alt={name} />
        <span>{name}</span>
      </Fragment>
    );
  }

  return <p>{name}</p>;
}

function renderPostImage(imageUrl: string, title: string) {
  if (imageUrl.length > 0) {
    return <img src={imageUrl} alt={title} />;
  }

  return (
    <div
      className={styles.articleBackground}
      style={{ backgroundColor: getRandomBackgroundColor() }}
    >
      {title}
    </div>
  );
}

export default Post;
