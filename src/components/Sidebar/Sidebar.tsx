import React from "react";
import { Link } from "react-router-dom";

import styles from './Sidebar.module.css';

const tags = [
    {
        title: "Michał",
        link: "/"
    },
    {
        title: "Napraw",
        link: "/"
    },
    {
        title: "Tagi",
        link: "/"
    },
];

function Sidebar(): JSX.Element {
    return (
        <aside className={styles.root}>
            <h2>Tagi</h2>
            <ul className={styles.tags}>
                {Array.from(new Set(tags)).map(item => <li key={item.title}><Link to={item.link}>{item.title}</Link></li>)}
            </ul>
        </aside>
    );
}

export default Sidebar;