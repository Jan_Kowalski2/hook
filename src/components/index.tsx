export { default as Nav } from "./Nav";
export { default as Header } from "./Header";
export { default as Footer } from "./Footer";
export { default as Sidebar } from "./Sidebar";
export { default as Post } from "./Post";