export const DEFAULT_FETCH_OPTIONS = {
  method: "GET",
  headers: {
    Accept: "application/json",
    "Content-Type": "application/json"
  }
};

export interface FetchStatus {
  isLoading: boolean;
  isLoaded: boolean;
  isError: boolean;
}