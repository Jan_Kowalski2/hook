import { DEFAULT_FETCH_OPTIONS } from "constants/index"

export function getRandomBackgroundColor(): string {
    const letters = ["0","1","2","3","4","5","6","7","8","9","A","B","C","D","E","F"];
    let color = "#";
    for (let i = 0; i < 6; i++) {
        color += letters[Math.round(Math.random() * 10)];
    }

    return color;
}

export function getDate(date: string, withHour: boolean = false): string {
    const timeDate = new Date(date);

    if (withHour) {
        return `${timeDate.toLocaleDateString("pl-PL")} ${timeDate.toLocaleTimeString("pl-PL", {hour: '2-digit', minute:'2-digit'})}.`;
    }

    return `${timeDate.toLocaleDateString("pl-PL")}.`; 
}

export function callAPI(endpoint: string, options: Object = {}): Promise<Object> {

    options = {
        ...DEFAULT_FETCH_OPTIONS,
        ...options,
    }

    return new Promise((resolve, reject) => {
        fetch(process.env.REACT_APP_API_HOST + endpoint, options)
            .then(response => {
                if (response.status === 200 || response.status === 201) {
                    return response.json().then(json => resolve({ response, json }))
                }
                else {
                    return reject({ response })
                }
            })
            .catch(err => reject(err));
    });
}