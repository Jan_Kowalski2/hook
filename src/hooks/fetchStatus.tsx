import { useState, useEffect } from "react";
import { callAPI } from "helpers";
import { FetchStatus } from "constants/index"

export function useFetchStatuses(
  endpoint: string,
  callback: Function,
): FetchStatus {
  const [fetchStatus, handleFetchStatus] = useState<FetchStatus>({
    isLoading: true,
    isLoaded: false,
    isError: false
  });

  useEffect(() => {
    if (fetchStatus.isLoading) {
      callAPI(endpoint)
        .then((response: any) => {
          callback(response);
          handleFetchStatus({
            ...fetchStatus,
            isLoaded: true,
            isLoading: false
          });
        })
        .catch(() =>
          handleFetchStatus({ ...fetchStatus, isError: true, isLoading: false })
        );
    }
  }, [fetchStatus.isLoading]);

  return fetchStatus;
}
