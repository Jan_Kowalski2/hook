import React, { useState, useEffect } from "react";
import { FetchStatus } from "constants/index";

const initialBlock = <span>Ładowanie...</span>;

export function ErrorView(): JSX.Element {
  return (
    <span>404</span>
  )
}

export function useSkeleton(
  fetchStatus: FetchStatus,
  Component: any,
  Skeleton?: any
): Function {
  const [Block, handleBlock] = useState(initialBlock);

  useEffect(() => {
    if (fetchStatus.isError) {
      return handleBlock(ErrorView);
    }
    if (fetchStatus.isLoading && Skeleton) {
      return handleBlock(Skeleton);
    }
    if (fetchStatus.isLoading && !Skeleton) {
      return handleBlock(initialBlock);
    }
    return handleBlock(Component);
  }, [fetchStatus.isLoading]);

  return () => Block;
}
