import React, { useState } from "react";

import { Sidebar, Post } from "components";
import { useFetchStatuses } from "hooks/fetchStatus";
import { useSkeleton } from "hooks/skeleton";

import styles from "./Home.module.css";

function Home(): JSX.Element {
  const [posts, handlePosts] = useState<Post[]>([]);
  const fetchStatus = useFetchStatuses("/posts?_embed", (response: any) => {
    handlePosts(response.json);
  });

  const Posts = posts.map(item => <Post key={item.id} fetchStatus={fetchStatus} isExtended={false} {...item} />)

  const Content = useSkeleton(fetchStatus, Posts);

  return (
    <section className={styles.root}>
      <div className="container">
        <section className={styles.main}>
          <Content />
        </section>
        <Sidebar />
      </div>
    </section>
  );
}

export default Home;
