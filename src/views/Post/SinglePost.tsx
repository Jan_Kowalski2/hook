import React, { useState } from "react";

import { Sidebar, Post } from "components";
import { useFetchStatuses } from "hooks/fetchStatus";
import { useSkeleton } from "hooks/skeleton";

import styles from "./SinglePost.module.css";

function SinglePost(props: any) {
  const [ post, handlePost ] = useState<any>(null);
  const postId = props.match.params.id;
  const fetchStatus = useFetchStatuses(`/posts/${postId}?_embed`, (response: any) => {
    handlePost(response.json);
    window.scrollTo(0,0);
  });

  const Content = useSkeleton(fetchStatus, <Post isExtended={true} fetchStatus={fetchStatus} {...post} />);

  return (
    <section className={styles.root}>
      <div className="container">
        <section className={styles.main}>
          <Content />
        </section>
        <Sidebar />
      </div>
    </section>
  );
}

export default SinglePost;
