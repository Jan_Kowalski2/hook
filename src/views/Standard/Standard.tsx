import React, { useState, useEffect } from "react";

import { Sidebar } from "components";
import { useFetchStatuses } from "hooks/fetchStatus";

import styles from "./Standard.module.css";

function Standard(props: any): JSX.Element {
  const [content, handleContent] = useState<any[]>([]);
  const pageUrl = props.location.pathname.substring(1)
  const fetchStatus = useFetchStatuses(`/pages/?slug=${pageUrl}`, (response: any) => {
    handleContent(response.json);
  });

  const renderContent = () => {
    if (fetchStatus.isError) {
      return (
        <div>Coś poszło nie tak.</div>
      )
    }
    if (fetchStatus.isLoaded) {
      return (
        <div>{content}</div>
      )
    }
    return <span>Ładowanie...</span>
  }

  return (
    <section className={styles.root}>
      <div className="container">
        <section className={styles.main}>
          {renderContent()}
        </section>
        <Sidebar />
      </div>
    </section>
  );
}

export default Standard;
